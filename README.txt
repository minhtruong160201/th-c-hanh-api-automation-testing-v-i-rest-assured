# THỰC HÀNH API AUTOMATION TESTING VỚI REST ASSURED
=== Giới thiệu project ===
Project Thực hành API Automation testing với rest assured là script automation test API theo 1 kịch bản cho trước
=== Các thành phần trong project ===
- 1 folder lab8 lưu trữ file java của project Thực hành API Automation testing với rest assured 
- 1 file word chứa yêu cầu và hưỡng dẫn cách hoàn thành project Thực hành API Automation testing với rest assured
=== Cách cài đặt ===
- Sử dụng phần mềm Eclipse IDE để làm việc với project
- Tại phần mềm Ecplipse IDE vào File -> Open Projects from File System or Archive -> chọn folder lab8
- Tại phần mềm Eclipse IDE vào Help -> Eclipse Marketplace -> tìm TestNG -> cài đặt -> restart phần mềm Eclipse IDE khi cài đặt xong testNG
=== Cách sử dụng ===
- Tại Package Explorer vào testng.xml -> click button Run as -> click button TestNG Suite
- Sau khi chạy test xong vào test-output -> html -> chuột phải file index.html -> Open With -> Web Browser để đọc kết quả testcase