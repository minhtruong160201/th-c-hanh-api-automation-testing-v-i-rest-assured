package lab8;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesFile {
	public static Object fetchProperties(String key) throws IOException {
		FileInputStream file = new FileInputStream("./configuration/config.properties");
		Properties property = new Properties();
		property.load(file);
		return property.get(key);
	}

}
