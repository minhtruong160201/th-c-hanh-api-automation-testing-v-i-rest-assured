package lab8;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class Lab8 {
	public Response response;
	String getEndPoint, param1, param2;
	
	@BeforeMethod
	public void getEndPoint() throws IOException {
		RestAssured.baseURI = (String) PropertiesFile.fetchProperties("URL");
		RestAssured.basePath = (String) PropertiesFile.fetchProperties("path");
		// tạo endpoint hoàn chỉnh từ baseURI và basePath 
		getEndPoint = RestAssured.baseURI + RestAssured.basePath;
		param1 = (String) PropertiesFile.fetchProperties("userId1");
		param2 = (String) PropertiesFile.fetchProperties("userId2");
	}
	
	@Test(priority = 0)
	public void TC_001() {
		System.out.println("Bắt đầu Testcase TC_001");
		response = RestAssured.get(getEndPoint + param1);
		// kiểm tra đúng statuscode hay ko
		assertEquals(response.getStatusCode(), 200, "Sai StatusCode: " + response.getStatusCode());
		if (response.getStatusCode() == 200) {
			System.out.println("Đúng StatusCode: " + response.getStatusCode());
		}
		System.out.println("kết thúc Testcase TC_001");
		System.out.println();
}
	@Test(priority = 1)
	public void TC_002() {
		System.out.println("Bắt đầu Testcase TC_002");
		response = RestAssured.get(getEndPoint + param1);
		// kiểm tra có trường id hay ko
		Assert.assertTrue(response.asPrettyString().contains("id"), "Không có trường id");
		if (response.asPrettyString().contains("id")) {
			System.out.println("Có trường id");
		}
		System.out.println("Kết thúc Testcase TC_002");
		System.out.println();
	}
	
	@Test(priority = 2)
	public void TC_003() {
		System.out.println("Bắt đầu Testcase TC_003");
		response = RestAssured.get(getEndPoint + param1);
		// kiểm tra có trường email hay ko
		Assert.assertTrue(response.asPrettyString().contains("email"), "Không có trường email");
		if (response.asPrettyString().contains("email")) {
			System.out.println("Có trường email");
		}
		System.out.println("Kết thúc Testcase TC_003");
		System.out.println();
	}
	
	@Test(priority = 3)
	public void TC_004() {
		System.out.println("Bắt đầu Testcase TC_004");
		response = RestAssured.get(getEndPoint + param1);
		// kiểm tra có trường first_name hay ko
		Assert.assertTrue(response.asPrettyString().contains("first_name"), "Không có trường first_name");
		if (response.asPrettyString().contains("first_name")) {
			System.out.println("Có trường first_name");
		}
		System.out.println("Kết thúc Testcase TC_004");
		System.out.println();
	}
	
	@Test(priority = 4)
	public void TC_005() {
		System.out.println("Bắt đầu Testcase TC_005");
		response = RestAssured.get(getEndPoint + param1);
		// kiểm tra có trường last_name hay ko
		Assert.assertTrue(response.asPrettyString().contains("last_name"), "Không có trường last_name");
		if (response.asPrettyString().contains("last_name")) {
			System.out.println("Có trường last_name");
		}
		System.out.println("Kết thúc Testcase TC_005");
		System.out.println();
	}
	
	@Test(priority = 5)
	public void TC_006() {
		System.out.println("Bắt đầu Testcase TC_006");
		response = RestAssured.get(getEndPoint + param1);
		// kiểm tra có trường avatar hay ko
		Assert.assertTrue(response.asPrettyString().contains("avatar"), "Không có trường avatar");
		if (response.asPrettyString().contains("avatar")) {
			System.out.println("Có trường avatar");
		}
		System.out.println("Kết thúc Testcase TC_006");
		System.out.println();
	}
	
	@Test(priority = 6)
	public void TC_007() {
		System.out.println("Bắt đầu Testcase TC_007");
		response = RestAssured.get(getEndPoint + param1);
		// kiểm tra id có giống nhau hay ko
		Assert.assertEquals(response.jsonPath().getString("data.id"), "2", "id không giống nhau");
		if (response.jsonPath().getString("data.id") == "2") {
			System.out.println("id giống nhau");
		}
		System.out.println("Kết thúc Testcase TC_007");
		System.out.println();
	}
	
	@Test(priority = 7)
	public void TC_008() {
		System.out.println("Bắt đầu Testcase TC_008");
		response = RestAssured.get(getEndPoint + param2);
		// kiểm tra statuscode có đúng hay ko
		Assert.assertEquals(response.getStatusCode(), 404, "Sai statusCode: " + response.getStatusCode());
		if (response.getStatusCode() == 404) {
			System.out.println("Đúng statusCode: " + response.getStatusCode());
		}
		System.out.println();
		System.out.println("Kết thúc Testcase TC_008");
	}
	
	@Test(priority = 8)
	public void TC_009() {
		System.out.println("Bắt đầu Testcase TC_09");
		response = RestAssured.get(getEndPoint + param2);
		// kiểm tra có trường message hay ko
		Assert.assertTrue(response.asPrettyString().contains("message"), "Không có trường message");
		if (response.asPrettyString().contains("message")) {
			System.out.println("Có trường message");
		}
		System.out.println("Kết thúc Testcase TC_009");
		System.out.println();
	}
	
	@Test(priority = 9)
	public void TC_010() {
		System.out.println("Bắt đầu Testcase TC_010");
		response = RestAssured.get(getEndPoint + param2);
		// kiểm tra nội dung trường message có đúng hay ko
		Assert.assertEquals(response.jsonPath().getString("message"), "User not found!" ,"Tồn tại user với id không có thật");
		if (response.jsonPath().getString("message") == "User not found!") {
			System.out.println("Đúng trường message khi user không có thật");
		}
		System.out.println("Kết thúc Testcase TC_010");
		System.out.println();
	}
}
